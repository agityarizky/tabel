<?php 
include_once('../header.php'); 
include_once('../koneksi.php'); 
?>


<div class="box">
	<h1>
		<small>Edit Data Kebutuhan Ruang Praktik</small>
		<div class="pull-right">
			<a href="tabel2.php" class="btn btn-warning btn-xs" role="button">Kembali</a>
		</div>
	</h1>
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<?php 
			$id = @$_GET['edit'];
			$sql_kebutuhanRuang = mysqli_query($koneksi, "SELECT * FROM kebutuhan_ruang_praktik WHERE Column1 = '$id'");
			$data = mysqli_fetch_array($sql_kebutuhanRuang);
			?>
			<form action="proses.php" method="post">
				<div class="form-group">
					<label for="No_KK">No KK</label>
					<input type="text" id="NoKK" name="NoKK" class="form-control"
					value="<?=$data['No_KK']?>">
				</div>
				<div class="form-group">
					<label for="Kompetensi Keahlian">Kompetensi Keahlian</label>
					<input type="text" name="KompetensiKeahlian" id="Kompetensi Keahlian" class="form-control" value="<?=$data['Kompetensi_Keahlian']?>">
				</div>
					<div class="form-group">
					<label for="Column1">Column1</label>
					<input type="text" name="Column1" id="Column1" class="form-control" required value="<?=$data['Column1']?>">
				</div>
				 <div class="form-group pull-right">
    				<input type="submit" name="edit" id="edit" class="btn btn-success" value="simpan">
  				</div>
			</form>
		</div>
	</div>
</div>

<?php include_once('../footer.php'); ?>