<?php include_once('../header.php'); ?>

   <div class="card mt-5">
                    <div class="card-body">
                        <h1 class="display-4">Kebutuhan Ruang Praktik</h1>
                    <table id="table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No KK</th>
                        <th>Kompetensi Keahlian</th>
                        <th>Column1</th>
                        <th></th>
                </thead>
                <tbody>
                    <?php 
                    include '../koneksi.php';
                    $kebutuhan_ruang_praktik = mysqli_query($koneksi,"select * from kebutuhan_ruang_praktik");
                    while ($row = mysqli_fetch_array($kebutuhan_ruang_praktik)) 
                    {
                        echo "<tr>
                        <td>".$row['No_KK']."</td>
                        <td>".$row['Kompetensi_Keahlian']."</td>
                        <td>".$row['Column1']."</td>
                        <td> <a class='btn btn-primary' href='edit.php?edit=".$row['Column1']."' role='button'>Edit</a></td>
                        </tr>";
                    }
                    ?>          
                </tbody>
            </table>

<?php include_once('../footer.php'); ?>
