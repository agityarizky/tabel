<?php include_once('../header.php'); ?>

<div class="card mt-5">
                    <div class="card-body">
                        <h1 class="display-4">Kebutuhan Minimum</h1>
                        <table id="table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>no</th>
                                    <th>kk</th>
                                    <th>No2</th>
                                    <th>Jenis</th>
                                    <th>Rasio</th>
                                    <th>Deskripsi</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                include '../koneksi.php';
                                $kebutuhan_minimum = mysqli_query($koneksi,"select * from kebutuhan_minimum");
                                while ($row = mysqli_fetch_array($kebutuhan_minimum)) 
                                {
                                    echo "<tr>
                                        <td>".$row['no']."</td>
                                        <td>".$row['kk']."</td>
                                        <td>".$row['No2']."</td>
                                        <td>".$row['Jenis']."</td>
                                        <td>".$row['Rasio']."</td>
                                        <td>".$row['Deskripsi']."</td>
                                        <td> <a class='btn btn-primary' href='edit.php?edit=".$row['No2']."' role='button'>Edit</a></td>
                                    </tr>";
                                }
                                ?>          
                            </tbody>
                        </table>
<?php include_once('../footer.php'); ?>

