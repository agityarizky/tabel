<?php 
include_once('../header.php'); 
include_once('../koneksi.php'); 
?>

<div class="box">
	<h1>
		<small>Edit Data Kebutuhan Minimum</small>
		<div class="pull-right">
			<a href="tabel1.php" class="btn btn-warning btn-xs" role="button">Kembali</a>
		</div>
	</h1>
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<?php 
			$id = @$_GET['edit'];
			$sql_kebutuhanMin = mysqli_query($koneksi, "SELECT * FROM kebutuhan_minimum WHERE No2 = '$id'");
			$data = mysqli_fetch_array($sql_kebutuhanMin);
			 ?>
			<form action="proses.php" method="post">
				<div class="form-group">
					<label for="no">No</label>
					<input type="text" name="no" id="n" class="form-control" value="<?=$data['no']?>">
				</div>
				<div class="form-group">
					<label for="kk">kk</label>
					<input type="text" name="kk" id="kk" class="form-control" value="<?=$data['kk']?>">
				</div>
				<div class="form-group">
					<label for="No2">No2</label>
					<input type="text" name="No2" id="No2" class="form-control" value="<?=$data['No2']?>"required>
				</div>
				<div class="form-group">
					<label for="Jenis">Jenis</label>
					<input type="text" name="Jenis" id="Jenis" class="form-control" value="<?=$data['Jenis']?>">
				</div>
				<div class="form-group">
					<label for="Rasio">Rasio</label>
					<input type="text" name="Rasio" id="Rasio" class="form-control" value="<?=$data['Rasio']?>">
				</div>
				<div class="form-group">
					<label for="Deskripsi">Deskripsi</label>
					<input type="text" name="Deskripsi" id="Deskripsi" class="form-control" value="<?=$data['Deskripsi']?>">
				</div>
				 <div class="form-group pull-right">
				 	<input type="submit" name="edit" id="edit" class="btn btn-success" value="simpan">
  				</div>
			</form>
		</div>
	</div>
</div>

<?php include_once('../footer.php'); ?>