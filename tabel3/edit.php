<?php 
include_once('../header.php'); 
include_once('../koneksi.php'); 
?>

<div class="box">
	<h1>
		<small>Edit Data Luasan Ruang Praktik Siswa</small>
		<div class="pull-right">
			<a href="tabel3.php" class="btn btn-warning btn-xs" role="button">Kembali</a>
		</div>
	</h1>
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<?php 
				$id = @$_GET['edit'];
				$sql_luasan = mysqli_query($koneksi, "SELECT * FROM luasan_ruang_praktik_siswa WHERE AreaKerja = '$id'") or die( mysqli_error($koneksi));
				$data = mysqli_fetch_array($sql_luasan);
			?>
			<form action="proses.php" method="post">
				<div class="form-group">
					<label for="NoKK">No KK</label>
					<input type="text" name="NoKK" id="NoKK" class="form-control"
					value="<?=$data['No_KK']?>">
				</div>
				<div class="form-group">
					<label for="KompetensiKeahlian">Kompetensi Keahlian</label>
					<input type="text" name="KompetensiKeahlian" id="KompetensiKeahlian" class="form-control" value="<?=$data['Kompetensi_Keahlian']?>">
				</div>
				<div class="form-group">
					<label for="AreaKerja">Area Kerja/ Laboratorium/ Ruang</label>
					<input type="text" name="AreaKerja" id="AreaKerja" class="form-control" value="<?=$data['AreaKerja']?>" required >
				</div>
				<div class="form-group">
					<label for="Rasio">Rasio</label>
					<input type="text" name="Rasio" id="Rasio" class="form-control" value="<?=$data['Rasio']?>">
				</div>
				<div class="form-group">
					<label for="Kapasitas">Kapasitas</label>
					<input type="text" name="Kapasitas" id="Kapasitas" class="form-control" value="<?=$data['Kapasitas']?>">
				</div>
				<div class="form-group">
					<label for="Luasan">Luasan (m2)</label>
					<input type="text" name="Luasan" id="Luasan" class="form-control" value="<?=$data['Luasan']?>">
				</div>
				<div class="form-group">
					<label for="TotalLuas">Total Luas (m2)</label>
					<input type="text" name="TotalLuas" id="TotalLuas" class="form-control" value="<?=$data['Total_Luas']?>">
				</div>
				<div class="form-group pull-right">
    				<input type="submit" name="edit" id="edit" class="btn btn-success" value="simpan">
  				</div>
			</form>
		</div>
	</div>
</div>

<?php include_once('../footer.php'); ?>