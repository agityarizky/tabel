<?php include_once('../header.php'); ?>

   <div class="card mt-5">
                    <div class="card-body">
                        <h1 class="display-4">Luasan Ruang Praktik Siswa</h1>
                    <table id="table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No KK</th>
                        <th>Kompetensi Keahlian</th>
                        <th>Area Kerja/ Laboratorium/ Ruang</th>
                        <th>Rasio</th>
                        <th>Kapasitas</th>
                        <th>Luasan (m2)</th>
                        <th>Total Luas (m2)</th>
                        <th></th>
                </thead>
                <tbody>
                    <?php 
                    include '../koneksi.php';
                    $luasan_ruang_praktik_siswa = mysqli_query($koneksi,"select * from luasan_ruang_praktik_siswa");
                    while ($row = mysqli_fetch_array($luasan_ruang_praktik_siswa)) 
                    {
                        echo "<tr>
                        <td>".$row['No_KK']."</td>
                        <td>".$row['Kompetensi_Keahlian']."</td>
                        <td>".$row['AreaKerja']."</td>
                        <td>".$row['Rasio']."</td>
                        <td>".$row['Kapasitas']."</td>
                        <td>".$row['Luasan']."</td>
                        <td>".$row['Total_Luas']."</td>
                        <td> <a class='btn btn-primary' href='edit.php?edit=".$row['AreaKerja']."' role='button'>Edit</a></td>
                        </tr>";
                    }
                    ?>          
                </tbody>
            </table>

<?php include_once('../footer.php'); ?>