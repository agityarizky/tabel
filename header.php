<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../assets/DataTables/Buttons-1.5.6/css/buttons.bootstrap4.min.css">
    <link href="../assets/css/simple-sidebar.css" rel="stylesheet">

    <title>Hello, world!</title>
</head>

<body>

    <div class="d-flex" id="wrapper">
   
         <!-- Sidebar -->
        <div class="bg-light border-right" id="sidebar-wrapper">
          <div class="sidebar-heading">Menu </div>
          <div class="list-group list-group-flush">
            <a href="../tabel1/tabel1.php" class="list-group-item list-group-item-action bg-light">Kebutuhan Minimum</a>
            <a href="../tabel2/tabel2.php" class="list-group-item list-group-item-action bg-light">Kebutuhan Ruang Praktik</a>
            <a href="../tabel3/tabel3.php" class="list-group-item list-group-item-action bg-light">Luasan Ruang Praktik Siswa</a>
            <a href="../tabel4/tabel4.php" class="list-group-item list-group-item-action bg-light">Alat Per Ruang Praktik</a>
          </div>
        </div>
        <!-- /#sidebar-wrapper -->
    
        <!-- Page Content -->
        <div id="page-content-wrapper">

            <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                <button class="btn btn-primary" id="menu-toggle">Toggle Menu</button>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>      
            </nav>

            <div class="container-fluid">