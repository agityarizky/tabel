<?php 
include_once('../header.php'); 
include_once('../koneksi.php'); 
?>

<div class="box">
	<h1>
		<small>Edit Data Alat per Ruang Praktik</small>
		<div class="pull-right">
			<a href="tabel4.php" class="btn btn-warning btn-xs" role="button">Kembali</a>
		</div>
	</h1>
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<?php 
				$id1 = @$_GET['edit1'];
				// echo "$id1";
				$id2 = @$_GET['edit2'];
				echo "$id2";
				$sql_alat = mysqli_query($koneksi, "SELECT * FROM alat_per_ruang_praktik WHERE AreaKerja = '$id1' AND No = '$id2'") or die ( mysqli_error($koneksi));
				$data = mysqli_fetch_array($sql_alat);
			?>
			<form action="proses.php" method="post">
				<div class="form-group">
					<label for="NoKK">No KK</label>
					<input type="text" id="NoKK" name="NoKK" class="form-control" value="<?=$data['No_KK']?>">
				</div>
				<div class="form-group">
					<label for="KompetensiKeahlian">Kompetensi Keahlian</label>
					<input type="text" name="KompetensiKeahlian" id="KompetensiKeahlian" class="form-control" value="<?=$data['Kompetensi_Keahlian']?>" >
				</div>
				<div class="form-group">
					<label for="AreaKerja">Area Kerja/ Laboratorium/ Ruang</label>
					<input type="text" name="AreaKerja" id="AreaKerja" class="form-control" value="<?=$data['AreaKerja']?>" required>
				</div>
				<div class="form-group">
					<label for="No">No</label>
					<input type="text" name="No" id="No" class="form-control" value="<?=$data['No']?>" required>
				</div>
				<div class="form-group">
					<label for="NamaAlat">Nama Alat</label>
					<input type="text" name="NamaAlat" id="NamaAlat" class="form-control" value="<?=$data['Nama_Alat']?>">
				</div>
				<div class="form-group">
					<label for="Deskripsi">Deskripsi Alat dan Spesifikasi</label>
					<textarea type="text" id="Deskripsi" name="Deskripsi" class="form-control" rows="20" cols="100"> value="<?=$data['Deskripsi_Alat_dan_Spesifikasi']?>"></textarea> 
				</div>
				<div class="form-group">
					<label for="Rasio">Rasio</label>
					<input type="text" name="Rasio" id="Rasio" class="form-control" value="<?=$data['Rasio']?>" >
				</div>
				<div class="form-group">
					<label for="Ilustrasi">Ilustrasi Alat</label>
					<input type="text" name="Ilustrasi" id="Ilustrasi" class="form-control" value="<?=$data['Ilustrasi_Alat']?>">
				</div>
				<div class="form-group">
					<label for="LevelTeknologi">Level Teknologi</label>
					<input type="text" name="LevelTeknologi" id="LevelTeknologi" class="form-control" value="<?=$data['Level_Teknologi']?>" >
				</div>
				<div class="form-group">
					<label for="LevelKeterampilan">Level Keterampilan</label>
					<input type="text" name="LevelKeterampilan" id="LevelKeterampilan" 
					class="form-control" value="<?=$data['Level_Keterampilan']?>"  >
				</div>
				<div class="form-group">
					<label for="NoGambar">No Gambar</label>
					<input type="text" name="NoGambar" id="NoGambar" class="form-control" value="<?=$data['No_Gambar']?>" >
				</div>
				<div class="form-group">
					<label for="DirektoriGambar">Direktori Gambar</label>
					<input type="text" name="DirektoriGambar" id="DirektoriGambar" class="form-control" value="<?=$data['Direktori_Gambar']?>" >
				</div>
				<div class="form-group">
					<label for="LinkGambar">Link Gambar</label>
					<input type="text" name="LinkGambar" id="LinkGambar" class="form-control" value="<?=$data['Link_Gambar']?>" >
				</div>
				<div class="form-group pull-right">
    				<input type="submit" name="edit" id="edit" class="btn btn-success" value="simpan">
  				</div>
			</form>
		</div>
	</div>
</div>

<?php include_once('../footer.php'); ?>