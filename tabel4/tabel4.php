<?php include_once('../header.php'); ?>

   <div class="card mt-5">
                    <div class="card-body">
                        <h1 class="display-4">Alat Per Ruang Praktik</h1>
                        <div class="table-responsive">
                            <table id="table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No KK</th>
                                        <th>Kompetensi Keahlian</th>
                                        <th>Area Kerja/ Laboratorium/ Ruang</th>
                                        <th>No</th>
                                        <th>Nama Alat</th>
                                        <th>Deskripsi Alat dan Spesifikasi</th>
                                        <th>Rasio</th>
                                        <th>Ilustrasi Alat</th>
                                        <th>Level Teknologi</th>
                                        <th>Level Keterampilan</th>
                                        <th>No Gambar</th>
                                        <th>Direktori Gambar</th>
                                        <th>Link Gambar</th>
                                        <th></th>
                                </thead>
                                <tbody>
                                    <?php 
                                    include '../koneksi.php';
                                    $alat_per_ruang_praktik = mysqli_query($koneksi,"select * from alat_per_ruang_praktik");
                                    while ($row = mysqli_fetch_array($alat_per_ruang_praktik)) 
                                    {
                                        echo "<tr>
                                        <td>".$row['No_KK']."</td>
                                        <td>".$row['Kompetensi_Keahlian']."</td>
                                        <td>".$row['AreaKerja']."</td>
                                        <td>".$row['No']."</td>
                                        <td>".$row['Nama_Alat']."</td>
                                        <td>".$row['Deskripsi_Alat_dan_Spesifikasi']."</td>
                                        <td>".$row['Rasio']."</td>
                                        <td>".$row['Ilustrasi_Alat']."</td>
                                        <td>".$row['Level_Teknologi']."</td>
                                        <td>".$row['Level_Keterampilan']."</td>
                                        <td>".$row['No_Gambar']."</td>
                                        <td>".$row['Direktori_Gambar']."</td>
                                        <td><img src=".$row['Link_Gambar']."></td>
                                        <td> <a class='btn btn-primary' href='edit.php?edit1=".$row['AreaKerja']."&edit2=".$row['No']."' role='button'>Edit</a></td>
                                        </tr>";
                                    }
                                    ?>          
                                </tbody>
                            </table>
                        </div>
<?php include_once('../footer.php'); ?>